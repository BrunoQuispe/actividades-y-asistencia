<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;
    protected $table = 'usuarios';

    protected $fillable = [
        'id_usuario', 'nombres', 'apellidos', 'codigo', 'modalidad', 'unidad', 'dni', 'privilegio', 'usuario', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
